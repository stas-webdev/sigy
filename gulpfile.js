const gulp = require('gulp');
const pug = require('gulp-pug');
const sass = require('gulp-sass');
const autoprefixer = require('gulp-autoprefixer');
const concat = require('gulp-concat');
const del = require('del');
const runSequence = require('run-sequence');
const path = require('path');

const srcDir = path.join(__dirname, 'src');
const assetsDir = path.join(__dirname, 'assets');
const distDir = path.join(__dirname, 'dist');

gulp.task('default', (done) => {
  return runSequence('clean', ['pug', 'js', 'sass', 'assets'], done);
});

gulp.task('watch', () => {
  return gulp.watch([srcDir + '/**/*.*', assetsDir + '/**/*.*'], ['default']);
});

gulp.task('clean', () => {
  return del([distDir + '/**', '!' + distDir]);
});

gulp.task('pug', () => {
  return gulp.src(srcDir + '/*.pug')
    .pipe(pug({ pretty: true }))
    .pipe(gulp.dest(distDir));
});

gulp.task('js', () => {
  return gulp.src(srcDir + '/**/*.js')
    .pipe(concat('scripts.js'))
    .pipe(gulp.dest(distDir));
});

gulp.task('sass', () => {
  return gulp.src(srcDir + '/*.sass')
    .pipe(sass({ outputStyle: 'expanded' }))
    .pipe(autoprefixer({
            browsers: ['last 5 versions'],
            cascade: false
        }))
    .pipe(gulp.dest(distDir));
});

gulp.task('assets', () => {
  return gulp.src(assetsDir + '/**/*.*')
    .pipe(gulp.dest(distDir));
});
