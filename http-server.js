const connect = require('connect');
const serveStatic = require('serve-static');
const http = require('http');
const path = require('path');

const publicDir = path.join(__dirname, 'dist');
const port = process.env.PORT || 3000;

const app = connect();

app.use(serveStatic(publicDir));

http.createServer(app).listen(port, (err) => {
  if (err) console.error(err);
  else console.log('App listening on port ' + port);
});
