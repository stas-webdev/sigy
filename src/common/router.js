(function () {

  document.addEventListener('DOMContentLoaded', initTriggerButtons);

  function initTriggerButtons () {
    const btns = document.querySelectorAll('[data-scroll-to]');
    btns.forEach(function (btn) {
      btn.addEventListener('click', onTriggerButtonClick);
    });
  }

  function onTriggerButtonClick (e) {
    var anchor = this.getAttribute('href');
    var targetEl = document.querySelector(anchor);
    if (!targetEl) return;
    e.preventDefault();
    var offset = getElOffset(targetEl);
    scrollWindowTo(offset.top, 500, function () {
      window.location.href = anchor;
    });
  }

  function getElOffset(el) {
    var rect = el.getBoundingClientRect(),
    scrollLeft = window.pageXOffset || document.documentElement.scrollLeft,
    scrollTop = window.pageYOffset || document.documentElement.scrollTop;
    return { top: rect.top + scrollTop, left: rect.left + scrollLeft }
  }

  function scrollWindowTo(to, duration, callback) {
      var element = window;
      var start = element.scrollY;
      var change = to - start;
      var currentTime = 0;
      var increment = 20;

      animateScroll();

      function animateScroll (){
        currentTime += increment;
        var val = easeInOutQuad(currentTime, start, change, duration);
        element.scrollTo({ top: val });
        if(currentTime < duration) {
            setTimeout(animateScroll, increment);
        } else if (callback) {
          callback.call();
        }
      }

      //t = current time
      //b = start value
      //c = change in value
      //d = duration
      function easeInOutQuad (t, b, c, d) {
        t /= d / 2;
        if (t < 1) return c / 2  * t * t + b;
        t--;
        return -c / 2 * (t * (t - 2) - 1) + b;
      };
  }

})();
