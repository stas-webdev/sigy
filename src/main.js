(function () {

  document.addEventListener('DOMContentLoaded', function () {
    initMainMenu();
  });

  function initMainMenu () {
    var menuBtn = document.getElementById('MainMenuBtn');
    var hamburger = menuBtn.querySelector('.hamburger');
    var menuOverlay = document.getElementById('MainMenuOverlay')
    var isMenuOpen = false;

    menuBtn.addEventListener('click', function (e) {
      e.preventDefault();
      if (isMenuOpen) closeMenu();
      else openMenu();
    });

    function openMenu () {
      menuBtn.classList.add('navbar-menu-btn--active');
      hamburger.classList.add('hamburger--active');
      menuOverlay.classList.add('navbar__menu--opened');
      isMenuOpen = true;
    }

    function closeMenu () {
      menuBtn.classList.remove('navbar-menu-btn--active');
      hamburger.classList.remove('hamburger--active');
      menuOverlay.classList.remove('navbar__menu--opened');
      isMenuOpen = false;
    }
  }

})();
